﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using System.Collections.Generic;

namespace TuprasOpt
{
    // ------------- PROCESS INSTANCE (CURRENT PRODUCTION STATE) -------------
    public class ProcessInstance
    {
        public List<InputFeed> InputFeeds { set; get; }             // The input feeds
        public List<OutputTank> OutputTanks { set; get; }        // The output tanks
        public Specifications Specs { set; get; }                // The production specs
        public OptSettings Settings { set; get; }                   // The optimization settings

        public ProcessInstance()
        {
            InputFeeds = new List<InputFeed>();
            OutputTanks = new List<OutputTank>();
            Specs = new Specifications();
            Settings = new OptSettings();
        }
    }

    public class InputFeed
    {
        public string InputNodeID { set; get; }                     // The input feed node
        public double IF_i { set; get; }                            // The incoming flow rate of the feed
        public double ISU_i { set; get; }                           // The incoming flow rate of SU
        public double IC2_i { set; get; }                           // The incoming flow rate of C2
        public double IC5_i { set; get; }                           // The incoming flow rate of C5
    }

    public class OutputTank
    {
        public string OutputNodeID { set; get; }                     // The output node corresponding to the pool
        public double Q_total_i { set; get; }                       // The total capacity of the pool
        public double Q_start_i { set; get; }                       // The amount of LPG in the LPG pool at the start of the recovery
        public double QC5_start_i { set; get; }                     // The amount of C5 in the LPG pool at the start of the recovery
        public double QC2_start_i { set; get; }                     // The amount of C2 in the LPG pool at the start of the recovery
        public double QSU_start_i { set; get; }                     // The amount of Sulphur in the LPG pool at the start of the recovery
    }

    // ------------- SPECIFICATIONS -------------

    public class Specifications
    {
        public double SU { set; get; }                            // Percentage specification for Sulphur content in LPG
        public double C2 { set; get; }                            // Percentage specification for C2 content in LPG
        public double C5 { set; get; }                            // Percentage specification for C5 content in LPG
        public double C2C5 { set; get; }                          // Percentage specification for C2 + C5 content in LPG
    }

    // ------------- SETTINGS FOR OPTIMIZATION -------------

    public class OptSettings
    {
        public double Horizon { set; get; }                         // Time horizon for recovery
        public double TimeToOptimize { set; get; }                  // Time provided for optimization 
        public double PriceOfLPG { set; get; }                      // Price of LPG (for level 3+ optimization model)
        public double PriceOfNG { set; get; }                       // Price of Natural Gas (for level 3+ optimization model)
        public double PriceOfElectricity { set; get; }              // Price of Electricity (for level 3+ optimization model)
    }
}
