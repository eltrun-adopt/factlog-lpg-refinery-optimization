﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using System.Collections.Generic;

namespace TuprasOpt
{
    public class OperationalScenarioParameters
    {
        public string OperationalScenarioID { set; get; }

        public string NodeID { set; get; }

        public Parameters OperParams { set; get; }

        public OperationalScenario OperScen { set; get; }
    }

    // -------------- OPERATIONAL PARAMETERS STRUCTURES -----------------------
    public class Parameters
    {
        public string NodeID { get; set; }
        public string ScenarioID_i_s { get; set; }
        public List<ScenarioParameter> ScenarioParameters { get; set; }
    }

    public class ScenarioParameter
    {
        public double Value { get; set; }
        public string Symbol { get; set; }
    }

    // -------------- OPERATIONAL SCENARIOS STRUCTURES -----------------------
    public class ListOperationalScenarios
    {
        public List<OperationalScenario> UnitScenarios { set; get; }

        public ListOperationalScenarios()
        {
            UnitScenarios = new List<OperationalScenario>();
        }
    }

    public class OperationalScenario
    {
        public string NodeID { set; get; }                            // NodeID
        public string ScenarioID_i_s { set; get; }                    // This is the common ID used for each scenario
        public double E_i_s { set; get; }                             // Energy consumed in this scenario
        public List<LinkScenario> LinkScenarios { set; get; }         // The implementation of each scenario to this link

        public OperationalScenario()
        {
            LinkScenarios = new List<LinkScenario>();
        }
    }

    // The scenario with respect to the link outgoing from the unit, i.e., what it does to each stream
    public class LinkScenario
    {
        public string LinkID { set; get; }              
        public double CAP_ij { set; get; }                             // The capacity of the link
        public double PF_i_j_s { set; get; }                           // Percentage of flow rate that flows through unit i towards unit j under this operational scenario
        public double PC5_i_j_s { set; get; }                          // Percentage of flow rate of C5 for unit i towards unit j running this operational scenario
        public double PC2_i_j_s { set; get; }                          // Percentage of flow rate of C2 for unit i towards unit j running this operational scenario
        public double PSU_i_j_s { set; get; }                          // Percentage of flow rate of Sulphur for unit i towards unit j running this operational scenario
    }
}
