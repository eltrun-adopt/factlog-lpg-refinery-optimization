﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using System;
using System.Collections.Generic;
using System.IO;
using Google.OrTools.LinearSolver;

namespace TuprasOpt
{
    public class OptStructure
    {
        // --- Process schema related structures ----
        public Dictionary<string, OptNode> AllNodes { set; get; }       // All nodes
        public List<OptNode> Proc { set; get; }                         // Nodes corresponding to processes
        public List<OptNode> Junc { set; get; }                         // Nodes corresponding to junctions
        public List<OptNode> Units { set; get; }                        // Proc \cup Junc, i.e., nodes corresponding to units
        public List<OptNode> Input { set; get; }                        // Nodes corresponding to Input
        public List<OptNode> Output { set; get; }                       // Nodes corresponding to Output
        public Dictionary<string, OptLink> AllLinks { set; get; }       // All links between nodes
        public OptLinksCounter OptLinksOverview { set; get; }           // Review of all types of links
        // --- Operational Scenario Related Structures ---

        // --- Instance related structures ----
        public ProcessInstance Instance { set; get; }                   // The input and output of production
        // ---- Solver related structures ------------
        public Solver MILPsolver { set; get; }
        public Objective ObjectiveFunction { set; get; }
        public List<Variable> AllVariables { set; get; }
        public List<Constraint> AllConstraints { set; get; }
        public Solution Sol { set; get; }
        // PATH TO OUTPUT - Compute for level 3 and up

        public OptStructure()
        {
            AllNodes = new Dictionary<string, OptNode>();
            Proc = new List<OptNode>();
            Junc = new List<OptNode>();
            Units = new List<OptNode>();
            Input = new List<OptNode>();
            Output = new List<OptNode>();
            AllLinks = new Dictionary<string, OptLink>();
            OptLinksOverview = new OptLinksCounter();
            AllVariables = new List<Variable>();
            AllConstraints = new List<Constraint>();
            Sol = new Solution();
        }

        public void InitializeProcessModel(Root TUCprocessSpec)
        {
            string energyID = "";
            // Get all nodes (process units, input, junctions and output)
            for (int i = 0; i < TUCprocessSpec.Definition.Nodes.Count; i++)
            {
                OptNode newOptPU = new OptNode();
                newOptPU.OptID = i.ToString();
                newOptPU.PSM_node = TUCprocessSpec.Definition.Nodes[i];
                AllNodes.Add(newOptPU.PSM_node.Id, newOptPU);

                if (TUCprocessSpec.Definition.Nodes[i].Type == "Process")
                {
                    if (newOptPU.PSM_node.Name.Contains("MQD"))
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.MQD;
                    }
                    else if (newOptPU.PSM_node.Name.Contains("Platformer"))
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.Platformer;
                    }
                    else if (newOptPU.PSM_node.Name.Contains("LPG DEA"))
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.LPG_DEA;
                    }
                    else if (newOptPU.PSM_node.Name.Contains("HYC DEA"))
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.LPG_DEA;
                    }
                    else if (newOptPU.PSM_node.Name.Contains("CDU"))
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.CDU;
                    }
                    else if (newOptPU.PSM_node.Name.Contains("HYC"))
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.HYC;
                    }
                    else if (newOptPU.PSM_node.Name.Contains("FCC"))
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.FCC;
                    }
                    else if (newOptPU.PSM_node.Name.Contains("DCU"))
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.DCU;
                    }
                    else if (newOptPU.PSM_node.Name.Contains("Tank"))
                    {
                        newOptPU.Type = OptUnitType.Output;
                        newOptPU.ProcessType = OptProcessType.Other;
                        Output.Add(newOptPU);
                    }
                    else if (newOptPU.PSM_node.Name.Contains("MIX"))
                    {
                        Units.Add(newOptPU);
                        Junc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Junction;
                        newOptPU.ProcessType = OptProcessType.Junction;
                    }
                    else
                    {
                        Units.Add(newOptPU);
                        Proc.Add(newOptPU);
                        newOptPU.Type = OptUnitType.Process;
                        newOptPU.ProcessType = OptProcessType.Other;
                        Console.WriteLine("Process with unknown ProcessType");
                    }
                }
                else if (TUCprocessSpec.Definition.Nodes[i].Type == "Junction")
                {
                    Units.Add(newOptPU);
                    Junc.Add(newOptPU);
                    newOptPU.Type = OptUnitType.Junction;
                    newOptPU.ProcessType = OptProcessType.Junction;
                }
                else if (TUCprocessSpec.Definition.Nodes[i].Type == "InputNode")
                {
                    if (TUCprocessSpec.Definition.Nodes[i].Name != "Energy")
                    {
                        newOptPU.Type = OptUnitType.Input;
                        newOptPU.ProcessType = OptProcessType.Other;
                        Input.Add(newOptPU);
                    }
                    else
                        energyID = TUCprocessSpec.Definition.Nodes[i].Id;
                }
                else // This is to check whether a new type of node has been introduced
                {
                    Console.WriteLine("Unit with unknown Type");
                }
            }

            // This is used to get an overview of all links

            // Store all links between nodes (process units, input, junctions and output)
            for (int i = 0; i < TUCprocessSpec.Definition.Links.Count; i++)
            {
                if (TUCprocessSpec.Definition.Links[i].Source != energyID)
                {
                    OptLink link = new OptLink(this, TUCprocessSpec.Definition.Links[i]);
                    link.Source.OutgoingLinks.Add(link);
                    link.Target.IncomingLinks.Add(link);
                    AllLinks.Add(link.TUClink.Id, link);
                    OptLinksOverview.AddLink(link.Source.Type, link.Target.Type);
                    var sour = AllNodes[link.Source.PSM_node.Id].PSM_node.Name + "__" + AllNodes[link.Source.PSM_node.Id].PSM_node.Id;
                    var targ = AllNodes[link.Target.PSM_node.Id].PSM_node.Name + "__" + AllNodes[link.Target.PSM_node.Id].PSM_node.Id;
                }
                else
                {
                    var test = AllNodes[TUCprocessSpec.Definition.Links[i].Target].PSM_node;
                }
            }
        }

        public void InitializeInputOutput(ProcessInstance pi)
        {
            for (int i = 0; i < pi.InputFeeds.Count; i++)
            {
                OptNode curInput = AllNodes[pi.InputFeeds[i].InputNodeID];
                curInput.IF = pi.InputFeeds[i];
            }
            for (int i = 0; i < pi.OutputTanks.Count; i++)
            {
                OptNode curOutput = AllNodes[pi.OutputTanks[i].OutputNodeID];
                curOutput.OT = pi.OutputTanks[i];
            }
        }

        public void InitializeOperationalScenarios(List<OperationalScenarioParameters> losp)
        {
            for (int i = 0; i < losp.Count; i++)
            {
                OptNode curNode = AllNodes[losp[i].OperScen.NodeID];
                OptUnitScenario ous = new OptUnitScenario();
                ous.ScenarioID_i_s = losp[i].OperScen.ScenarioID_i_s;
                ous.E_i_s = losp[i].OperScen.E_i_s;
                for (int j = 0; j < losp[i].OperScen.LinkScenarios.Count; j++)
                {
                    OptLinkScenario ols = new OptLinkScenario();
                    ols.Scenario_i = ous;
                    ols.PF_i_j_s = losp[i].OperScen.LinkScenarios[j].PF_i_j_s;
                    ols.PSU_i_j_s = losp[i].OperScen.LinkScenarios[j].PSU_i_j_s;
                    ols.PC2_i_j_s = losp[i].OperScen.LinkScenarios[j].PC2_i_j_s;
                    ols.PC5_i_j_s = losp[i].OperScen.LinkScenarios[j].PC5_i_j_s;
                    ols.Link = AllLinks[losp[i].OperScen.LinkScenarios[j].LinkID];
                    //ols.Link.CAP_ij = losp[i].OperScen.LinkScenarios[j].CAP_ij;
                    ols.Link.CAP_ij = 10000;
                    ols.Link.Scenarios_i_j_s.Add(ols);
                    ous.LinkScenarios.Add(ols);
                }
                curNode.UnitScenarios.Add(ous);
            }

            foreach (var node in AllNodes.Values)
                if (node.Type == OptUnitType.Junction)
                {
                    OptUnitScenario ous = new OptUnitScenario();
                    ous.ScenarioID_i_s = "Junction";
                    ous.E_i_s = 0;
                    OptLinkScenario ols = new OptLinkScenario();
                    ols.Scenario_i = ous;
                    ols.PF_i_j_s = 1;
                    ols.PSU_i_j_s = 1;
                    ols.PC2_i_j_s = 1;
                    ols.PC5_i_j_s = 1;
                    foreach (var link in AllLinks.Values)
                        if (link.Source.PSM_node.Id == node.PSM_node.Id)
                            ols.Link = link;
                    ols.Link.CAP_ij = 10000;
                    ols.Link.Scenarios_i_j_s.Add(ols);
                    ous.LinkScenarios.Add(ols);
                    node.UnitScenarios.Add(ous);
                }
        }

        public void RunParetoFilter()
        {
            for (int i = 0; i < Proc.Count; i++)
            {
                int j = 0;
                while (j < Proc[i].UnitScenarios.Count)
                {
                    bool removedJ = false;
                    int k = j + 1;
                    while (k < Proc[i].UnitScenarios.Count)
                    {
                        if ((Proc[i].UnitScenarios[j].E_i_s <= Proc[i].UnitScenarios[k].E_i_s) &&
                            (Proc[i].UnitScenarios[j].LinkScenarios[0].PC2_i_j_s <= Proc[i].UnitScenarios[k].LinkScenarios[0].PC2_i_j_s) &&
                            (Proc[i].UnitScenarios[j].LinkScenarios[0].PC5_i_j_s <= Proc[i].UnitScenarios[k].LinkScenarios[0].PC5_i_j_s) &&
                            (Proc[i].UnitScenarios[j].LinkScenarios[0].PSU_i_j_s <= Proc[i].UnitScenarios[k].LinkScenarios[0].PSU_i_j_s) &&
                            (Proc[i].UnitScenarios[j].LinkScenarios[0].PF_i_j_s >= Proc[i].UnitScenarios[k].LinkScenarios[0].PF_i_j_s))
                            Proc[i].UnitScenarios.RemoveAt(k);
                        else if ((Proc[i].UnitScenarios[j].E_i_s >= Proc[i].UnitScenarios[k].E_i_s) &&
                            (Proc[i].UnitScenarios[j].LinkScenarios[0].PC2_i_j_s >= Proc[i].UnitScenarios[k].LinkScenarios[0].PC2_i_j_s) &&
                            (Proc[i].UnitScenarios[j].LinkScenarios[0].PC5_i_j_s >= Proc[i].UnitScenarios[k].LinkScenarios[0].PC5_i_j_s) &&
                            (Proc[i].UnitScenarios[j].LinkScenarios[0].PSU_i_j_s >= Proc[i].UnitScenarios[k].LinkScenarios[0].PSU_i_j_s) &&
                            (Proc[i].UnitScenarios[j].LinkScenarios[0].PF_i_j_s <= Proc[i].UnitScenarios[k].LinkScenarios[0].PF_i_j_s))
                        {
                            Proc[i].UnitScenarios.RemoveAt(j);
                            removedJ = true;
                            break;
                        }                        
                        else
                            k++;
                    }
                    if (!removedJ)
                        j++;
                }
            }
        }

        public void CreateSolver(string solverStr)
        {
            MILPsolver = Solver.CreateSolver(solverStr);
        }

        public void InstantiateMILPvariables()
        {
            // Initiate the output variables
            for (int i = 0; i < Output.Count; i++)
            {
                var output = Output[i];
                output.OutputVars.q_i = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "q_" + output.OptID);
                output.OutputVars.qSU_i = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "qSU_" + output.OptID);
                output.OutputVars.qC2_i = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "qC2_" + output.OptID);
                output.OutputVars.qC5_i = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "qC5_" + output.OptID);
                AllVariables.Add(output.OutputVars.q_i);
                AllVariables.Add(output.OutputVars.qSU_i);
                AllVariables.Add(output.OutputVars.qC2_i);
                AllVariables.Add(output.OutputVars.qC5_i);
            }
            // Initiate scenario decision variables per unit
            for (int k = 0; k < Units.Count; k++)
                for (int i = 0; i < Units[k].UnitScenarios.Count; i++)
                {
                    Units[k].UnitScenarios[i].x_i_s = MILPsolver.MakeBoolVar("x_" + Units[k].OptID + "_" + i.ToString());
                    AllVariables.Add(Units[k].UnitScenarios[i].x_i_s);
                }

            // Initiate link flow and corresdponding scenario flow variables
            for (int i = 0; i < Input.Count; i++)
                for (int j = 0; j < Input[i].OutgoingLinks.Count; j++)
                {
                    var link = Input[i].OutgoingLinks[j];
                    link.fstar_i_j = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fstar_" + link.Source.OptID + "_" + link.Target.OptID);
                    link.fSUstar_i_j = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fSUstar_" + link.Source.OptID + "_" + link.Target.OptID);
                    link.fC2star_i_j = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fC2star_" + link.Source.OptID + "_" + link.Target.OptID);
                    link.fC5star_i_j = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fC5star_" + link.Source.OptID + "_" + link.Target.OptID);
                    AllVariables.Add(link.fstar_i_j);
                    AllVariables.Add(link.fSUstar_i_j);
                    AllVariables.Add(link.fC2star_i_j);
                    AllVariables.Add(link.fC5star_i_j);
                }
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    var link = Units[i].OutgoingLinks[j];
                    link.fstar_i_j = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fstar_" + link.Source.OptID + "_" + link.Target.OptID);
                    link.fSUstar_i_j = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fSUstar_" + link.Source.OptID + "_" + link.Target.OptID);
                    link.fC2star_i_j = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fC2star_" + link.Source.OptID + "_" + link.Target.OptID);
                    link.fC5star_i_j = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fC5star_" + link.Source.OptID + "_" + link.Target.OptID);
                    AllVariables.Add(link.fstar_i_j);
                    AllVariables.Add(link.fSUstar_i_j);
                    AllVariables.Add(link.fC2star_i_j);
                    AllVariables.Add(link.fC5star_i_j);
                    for (int k = 0; k < link.Scenarios_i_j_s.Count; k++)
                    {
                        link.Scenarios_i_j_s[k].f_i_j_s = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "f_" + link.Source.OptID + "_" + link.Target.OptID + "_" + k.ToString());
                        link.Scenarios_i_j_s[k].fSU_i_j_s = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fSU_" + link.Source.OptID + "_" + link.Target.OptID + "_" + k.ToString());
                        link.Scenarios_i_j_s[k].fC2_i_j_s = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fC2_" + link.Source.OptID + "_" + link.Target.OptID + "_" + k.ToString());
                        link.Scenarios_i_j_s[k].fC5_i_j_s = MILPsolver.MakeNumVar(0.0, double.PositiveInfinity, "fC5_" + link.Source.OptID + "_" + link.Target.OptID + "_" + k.ToString());
                        AllVariables.Add(link.Scenarios_i_j_s[k].f_i_j_s);
                        AllVariables.Add(link.Scenarios_i_j_s[k].fSU_i_j_s);
                        AllVariables.Add(link.Scenarios_i_j_s[k].fC2_i_j_s);
                        AllVariables.Add(link.Scenarios_i_j_s[k].fC5_i_j_s);
                    }
                }
        }

        public void BuildMILPconstraints()
        {
            string con = "";
            int counter = -1;
            // Constraint 1
            for (int i = 0; i < Units.Count; i++)
            {
                con = "c1_";
                AllConstraints.Add(MILPsolver.MakeConstraint(1, 1, con + Units[i].OptID));
                counter++;
                for (int j = 0; j < Units[i].UnitScenarios.Count; j++)
                    AllConstraints[counter].SetCoefficient(Units[i].UnitScenarios[j].x_i_s, 1);
            }
            // Constraint 2
            for (int i = 0; i < Input.Count; i++)
            {
                con = "c2_";
                AllConstraints.Add(MILPsolver.MakeConstraint(Input[i].IF.IF_i, Input[i].IF.IF_i, con + Input[i].OptID));
                counter++;
                if (Input[i].OutgoingLinks.Count == 1)
                    AllConstraints[counter].SetCoefficient(Input[i].OutgoingLinks[0].fstar_i_j, 1);
                else
                    Console.WriteLine("Problem with input feed");
            }
            // Constraint 3
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c3_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, 0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].fstar_i_j, 1);
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].f_i_j_s, -1);
                }
            // Constraint 4
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                    {
                        con = "c4_";
                        AllConstraints.Add(MILPsolver.MakeConstraint(double.NegativeInfinity, 0.0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID + "_" + k));
                        counter++;
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].f_i_j_s, 1);
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].Scenario_i.x_i_s, -1 * Units[i].OutgoingLinks[j].CAP_ij);
                    }
            // Constraint 5
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c5_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, 0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    for (int k = 0; k < Units[i].IncomingLinks.Count; k++)
                        AllConstraints[counter].SetCoefficient(Units[i].IncomingLinks[k].fstar_i_j, 1);
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                        if (Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].PF_i_j_s != 0)
                        {
                            double coef = -1 * Math.Round(1 / Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].PF_i_j_s, 3);
                            AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].f_i_j_s, coef);
                        }
                        else
                            Console.WriteLine("Constraint 5 - PF_i_j_s = 0");
                }
            // Constraint 6
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c6_";
                AllConstraints.Add(MILPsolver.MakeConstraint(Output[i].OT.Q_start_i, Output[i].OT.Q_start_i, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.q_i, 1);
                for (int k = 0; k < Output[i].IncomingLinks.Count; k++)
                    AllConstraints[counter].SetCoefficient(Output[i].IncomingLinks[k].fstar_i_j, -1 * Instance.Settings.Horizon);
            }
            // Constraint 7
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c7_";
                AllConstraints.Add(MILPsolver.MakeConstraint(0, Output[i].OT.Q_total_i, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.q_i, 1);
            }
            // Constraint 8
            for (int i = 0; i < Input.Count; i++)
            {
                con = "c8_";
                AllConstraints.Add(MILPsolver.MakeConstraint(Input[i].IF.ISU_i, Input[i].IF.ISU_i, con + Input[i].OptID));
                counter++;
                if (Input[i].OutgoingLinks.Count == 1)
                    AllConstraints[counter].SetCoefficient(Input[i].OutgoingLinks[0].fSUstar_i_j, 1);
                else
                    Console.WriteLine("Problem with input feed");
            }
            // Constraint 9
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c9_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, 0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].fSUstar_i_j, 1);
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fSU_i_j_s, -1);
                }
            // Constraint 10
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                    {
                        con = "c10_";
                        AllConstraints.Add(MILPsolver.MakeConstraint(double.NegativeInfinity, 0.0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID + "_" + k));
                        counter++;
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fSU_i_j_s, 1);
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].f_i_j_s, -1);
                    }
            // Constraint 11
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c11_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, 0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    for (int k = 0; k < Units[i].IncomingLinks.Count; k++)
                        AllConstraints[counter].SetCoefficient(Units[i].IncomingLinks[k].fSUstar_i_j, 1);
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                        if (Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].PSU_i_j_s != 0)
                        {
                            double coef = -1 * Math.Round(1 / Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].PSU_i_j_s, 3);
                            AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fSU_i_j_s, coef);
                        }
                        else
                            Console.WriteLine("Constraint 11 - PSU_i_j_s = 0");
                }

            double density = 1;
            // multiply volume by density to get weight
            // Constraint 12
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c12_";
                AllConstraints.Add(MILPsolver.MakeConstraint(density * Output[i].OT.QSU_start_i, density * Output[i].OT.QSU_start_i, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.qSU_i, 1);
                for (int k = 0; k < Output[i].IncomingLinks.Count; k++)
                    AllConstraints[counter].SetCoefficient(Output[i].IncomingLinks[k].fSUstar_i_j, -1 * Instance.Settings.Horizon);
            }
            // Constraint 13
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c13_";
                AllConstraints.Add(MILPsolver.MakeConstraint(double.NegativeInfinity, 0, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.qSU_i, 1);
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.q_i, -1 * density * Instance.Specs.SU);
            }
            // Constraint 14
            for (int i = 0; i < Input.Count; i++)
            {
                con = "c14_";
                AllConstraints.Add(MILPsolver.MakeConstraint(Input[i].IF.IC2_i, Input[i].IF.IC2_i, con + Input[i].OptID));
                counter++;
                if (Input[i].OutgoingLinks.Count == 1)
                    AllConstraints[counter].SetCoefficient(Input[i].OutgoingLinks[0].fC2star_i_j, 1);
                else
                    Console.WriteLine("Problem with input feed");
            }
            // Constraint 15
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c15_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, 0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].fC2star_i_j, 1);
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fC2_i_j_s, -1);
                }
            // Constraint 16
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                    {
                        con = "c16_";
                        AllConstraints.Add(MILPsolver.MakeConstraint(double.NegativeInfinity, 0.0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID + "_" + k));
                        counter++;
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fC2_i_j_s, 1);
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].f_i_j_s, -1);
                    }
            // Constraint 17
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c17_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, 0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    for (int k = 0; k < Units[i].IncomingLinks.Count; k++)
                        AllConstraints[counter].SetCoefficient(Units[i].IncomingLinks[k].fC2star_i_j, 1);
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                        if (Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].PC2_i_j_s != 0)
                        {
                            double coef = -1 * Math.Round(1 / Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].PC2_i_j_s, 3);
                            AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fC2_i_j_s, coef);
                        }
                        else
                            Console.WriteLine("Constraint 17 - PC2_i_j_s = 0");
                }
            // Constraint 18
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c18_";
                AllConstraints.Add(MILPsolver.MakeConstraint(Output[i].OT.QC2_start_i, Output[i].OT.QC2_start_i, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.qC2_i, 1);
                for (int k = 0; k < Output[i].IncomingLinks.Count; k++)
                    AllConstraints[counter].SetCoefficient(Output[i].IncomingLinks[k].fC2star_i_j, -1 * Instance.Settings.Horizon);
            }
            //// Constraint 19
            //for (int i = 0; i < Output.Count; i++)
            //{
            //    con = "c19_";
            //    AllConstraints.Add(MILPsolver.MakeConstraint(double.NegativeInfinity, 0, con + Output[i].OptID));
            //    counter++;
            //    AllConstraints[counter].SetCoefficient(Output[i].OutputVars.qC2_i, 1);
            //    AllConstraints[counter].SetCoefficient(Output[i].OutputVars.q_i, -1 * Instance.Specs.C2);
            //}
            // Constraint 20
            for (int i = 0; i < Input.Count; i++)
            {
                con = "c20_";
                AllConstraints.Add(MILPsolver.MakeConstraint(Input[i].IF.IC5_i, Input[i].IF.IC5_i, con + Input[i].OptID));
                counter++;
                if (Input[i].OutgoingLinks.Count == 1)
                    AllConstraints[counter].SetCoefficient(Input[i].OutgoingLinks[0].fC5star_i_j, 1);
                else
                    Console.WriteLine("Problem with input feed");
            }
            // Constraint 21
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c21_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, 0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].fC5star_i_j, 1);
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fC5_i_j_s, -1);
                }
            // Constraint 22
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                    {
                        con = "c22_";
                        AllConstraints.Add(MILPsolver.MakeConstraint(double.NegativeInfinity, 0.0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID + "_" + k));
                        counter++;
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fC5_i_j_s, 1);
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].f_i_j_s, -1);
                    }
            // Constraint 23
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c23_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, 0, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    for (int k = 0; k < Units[i].IncomingLinks.Count; k++)
                        AllConstraints[counter].SetCoefficient(Units[i].IncomingLinks[k].fC5star_i_j, 1);
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                        if (Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].PC5_i_j_s != 0)
                        {
                            double coef = -1 * Math.Round(1 / Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].PC5_i_j_s, 3);
                            AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].fC5_i_j_s, coef);
                        }
                        else
                            Console.WriteLine("Constraint 23 - PC5_i_j_s = 0");
                }
            // Constraint 24
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c24_";
                AllConstraints.Add(MILPsolver.MakeConstraint(Output[i].OT.QC5_start_i, Output[i].OT.QC5_start_i, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.qC5_i, 1);
                for (int k = 0; k < Output[i].IncomingLinks.Count; k++)
                    AllConstraints[counter].SetCoefficient(Output[i].IncomingLinks[k].fC5star_i_j, -1 * Instance.Settings.Horizon);
            }
            // Constraint 25
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c25_";
                AllConstraints.Add(MILPsolver.MakeConstraint(double.NegativeInfinity, 0, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.qC5_i, 1);
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.q_i, -1 * Instance.Specs.C5);
            }
            // Constraint 26
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c26_";
                AllConstraints.Add(MILPsolver.MakeConstraint(double.NegativeInfinity, 0, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.qC2_i, 1);
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.qC5_i, 1);
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.q_i, -1 * Instance.Specs.C2C5);
            }
            // Constraint 27: 
            // x_i_s \in {0,1}
            // Constraint 28
            for (int i = 0; i < Output.Count; i++)
            {
                con = "c28_";
                AllConstraints.Add(MILPsolver.MakeConstraint(0, double.PositiveInfinity, con + Output[i].OptID));
                counter++;
                AllConstraints[counter].SetCoefficient(Output[i].OutputVars.q_i, 1);
            }

            // Constraint 29
            for (int i = 0; i < Input.Count; i++)
                for (int j = 0; j < Input[i].OutgoingLinks.Count; j++)
                {
                    con = "c29a_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, double.PositiveInfinity, con + Input[i].OptID + "_" + Input[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    AllConstraints[counter].SetCoefficient(Input[i].OutgoingLinks[j].fstar_i_j, 1);
                }
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                {
                    con = "c29b_";
                    AllConstraints.Add(MILPsolver.MakeConstraint(0, double.PositiveInfinity, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID));
                    counter++;
                    AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].fstar_i_j, 1);
                }

            // Constraint 30
            for (int i = 0; i < Units.Count; i++)
                for (int j = 0; j < Units[i].OutgoingLinks.Count; j++)
                    for (int k = 0; k < Units[i].OutgoingLinks[j].Scenarios_i_j_s.Count; k++)
                    {
                        con = "c30_";
                        AllConstraints.Add(MILPsolver.MakeConstraint(0, double.PositiveInfinity, con + Units[i].OptID + "_" + Units[i].OutgoingLinks[j].Target.OptID + "_" + k));
                        counter++;
                        AllConstraints[counter].SetCoefficient(Units[i].OutgoingLinks[j].Scenarios_i_j_s[k].f_i_j_s, 1);
                    }
        }

        public void BuildMILPobjective()
        {
            ObjectiveFunction = MILPsolver.Objective();

            for (int i = 0; i < Proc.Count; i++)
                for (int j = 0; j < Proc[i].UnitScenarios.Count; j++)
                    ObjectiveFunction.SetCoefficient(Proc[i].UnitScenarios[j].x_i_s, Instance.Settings.Horizon * Proc[i].UnitScenarios[j].E_i_s);

            ObjectiveFunction.SetMinimization();
        }

        public void SolveMILP()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var resultStatus = MILPsolver.Solve();
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Sol.SolKPIs.TimeToSolveMillisec = Convert.ToDouble(elapsedMs);
            if (resultStatus.ToString() == "OPTIMAL")
            {
                Sol.SolKPIs.FoundSolution = true;
                Sol.TotalEnergy = MILPsolver.Objective().Value();
                double totalEnergyCalc = 0;
                for (int i = 0; i < Proc.Count; i++)
                    for (int j = 0; j < Proc[i].UnitScenarios.Count; j++)
                        if (Proc[i].UnitScenarios[j].x_i_s.SolutionValue() == 1)
                        {
                            totalEnergyCalc += Proc[i].UnitScenarios[j].x_i_s.SolutionValue() * Instance.Settings.Horizon * Proc[i].UnitScenarios[j].E_i_s;
                            SelectedScenario sel = new SelectedScenario();
                            sel.NodeID = Proc[i].PSM_node.Id;
                            sel.ScenarioID = Proc[i].UnitScenarios[j].ScenarioID_i_s;
                            sel.OptID = Proc[i].OptID;
                            Sol.SolutionScenarios.Add(sel);
                        }

                double density = 1;
                for (int i = 0; i < Output.Count; i++)
                {
                    var output = Output[i];
                    SpecsKPIs kpi = new SpecsKPIs();
                    kpi.OutputNodeID = output.PSM_node.Id;
                    kpi.Quantity = Math.Round(output.OutputVars.q_i.SolutionValue(), 3);
                    kpi.SUperc = Math.Round(1000000*(output.OutputVars.qSU_i.SolutionValue() / (density * output.OutputVars.q_i.SolutionValue())), 3);
                    kpi.C2perc = Math.Round(output.OutputVars.qC2_i.SolutionValue() / output.OutputVars.q_i.SolutionValue(), 4);
                    kpi.C5perc = Math.Round(output.OutputVars.qC5_i.SolutionValue() / output.OutputVars.q_i.SolutionValue(), 4);
                    kpi.C2C5perc = Math.Round(kpi.C2perc + kpi.C5perc, 4);
                    Sol.OutputKPIs.Add(kpi);
                }
            }
            else if (resultStatus.ToString() == "INFEASIBLE")
            {
                Sol.SolKPIs.FoundSolution = true;
            }
        }
    }

    public class OptNode
    {
        public string OptID { set; get; }                                  // Optimization inner node ID
        public Node PSM_node { set; get; }                               // The node as provided by TUC
        public OptUnitType Type { set; get; }                           // The type of the node (Process, Junction, Input, Output)
        public OptProcessType ProcessType { set; get; }                 // The process type (this is only for processes and junctions)
        public List<OptLink> IncomingLinks { set; get; }                // List of incoming links
        public List<OptLink> OutgoingLinks { set; get; }                // List of outgoing links
        // ------------- INPUT AND OUTPUT ------
        public InputFeed IF { set; get; }
        public OutputTank OT { set; get; }
        // ------------- SCENARIOS -------------
        public List<OptUnitScenario> UnitScenarios { set; get; }        // List of all operational scenarios of this node - Initiated only for units ---
        // ------------- VARIABLES -------------
        public VarNodeOutput OutputVars { set; get; }
        // ------------------------------------

        public OptNode()
        {
            IncomingLinks = new List<OptLink>();
            OutgoingLinks = new List<OptLink>();
            UnitScenarios = new List<OptUnitScenario>();
            OutputVars = new VarNodeOutput();
        }
    }

    public class VarNodeOutput
    {
        // ------------- VARIABLES -------------
        public Variable q_i { set; get; }                             // Variable: The amount of LPG in the final pool
        public Variable qC5_i { set; get; }                           // Variable: The amount of C5 in the final pool
        public Variable qC2_i { set; get; }                           // Variable: The amount of C5 in the final pool
        public Variable qSU_i { set; get; }                           // Variable: The amount of C5 in the final pool
        // ------------------------------------
    }

    public class OptLink
    {
        public Link TUClink { set; get; }                           // The link as provided by TUC
        public OptNode Source { set; get; }                         // The source node
        public OptNode Target { set; get; }                         // The target node
        public bool PathToOutput { set; get; }                      // True if the link is on at least one path to an output node
        // ------------- SCENARIOS -------------
        public double CAP_ij { set; get; }                          // The capacity of the link
        public List<OptLinkScenario> Scenarios_i_j_s { set; get; }     // The implementation of each scenario to this link

        // ------------- VARIABLES -------------
        public Variable fstar_i_j { set; get; }                        // Variable: Flow rate of LPG through this link
        public Variable fSUstar_i_j { set; get; }                      // Variable: Flow rate of Sulphur through this link
        public Variable fC2star_i_j { set; get; }                      // Variable: Flow rate of C2 through this link
        public Variable fC5star_i_j { set; get; }                      // Variable: Flow rate of C5 through this link
        // ------------------------------------

        public OptLink(OptStructure opt, Link link)
        {
            TUClink = link;
            Source = opt.AllNodes[link.Source];
            Target = opt.AllNodes[link.Target];
            PathToOutput = true; // This may not be always true (for level 3 and 4)
            Scenarios_i_j_s = new List<OptLinkScenario>();
        }
    }

    // ------------- OPERATIONAL SCENARIOS -------------

    // The scenario with respect to the unit
    public class OptUnitScenario
    {
        public string ScenarioID_i_s { set; get; }                    // This is the common ID used for each scenario
        public double E_i_s { set; get; }                             // Energy consumed in this scenario
        public List<OptLinkScenario> LinkScenarios { set; get; }
        // ------------- VARIABLES -------------
        public Variable x_i_s { set; get; }                           // Variable: True, if the scenario is selected
        // ------------------------------------

        public OptUnitScenario()
        {
            LinkScenarios = new List<OptLinkScenario>();
        }
    }

    // The scenario with respect to the link outgoing from the unit, i.e., what it does to each stream
    public class OptLinkScenario
    {
        public OptLink Link { set; get; }                               // The corresponding link
        public OptUnitScenario Scenario_i { set; get; }                 // The scenario with respect to the unit
        public double PF_i_j_s { set; get; }                            // Percentage of flow rate that flows through unit i towards unit j under this operational scenario
        public double PC5_i_j_s { set; get; }                           // Percentage of flow rate of C5 for unit i towards unit j running this operational scenario
        public double PC2_i_j_s { set; get; }                           // Percentage of flow rate of C2 for unit i towards unit j running this operational scenario
        public double PSU_i_j_s { set; get; }                           // Percentage of flow rate of Sulphur for unit i towards unit j running this operational scenario
        // ------------- VARIABLES -------------
        public Variable f_i_j_s { set; get; }                            // Variable: Flow rate of LPG through this link in this scenario
        public Variable fSU_i_j_s { set; get; }                          // Variable: Flow rate of Sulphur through this link in this scenario
        public Variable fC2_i_j_s { set; get; }                          // Variable: Flow rate of C2 through this link in this scenario
        public Variable fC5_i_j_s { set; get; }                          // Variable: Flow rate of C5 through this link in this scenario
        // ------------------------------------
    }

    // ------------- TYPES OF UNITS AND PROCESSES -------------

    public enum OptUnitType
    {
        Process,
        Junction,
        Input,
        Output
    }

    public enum OptProcessType
    {
        CDU,
        HYC,
        FCC,
        DCU,
        MQD,
        Platformer,
        Junction,
        LPG_DEA,
        Other
    }

    // ------------- USED TO STORE THE TYPE OF LINKS INCLUDED IN PROCESS MODEL -------------

    public class OptTypeOfLinks
    {
        public OptUnitType SourceType { set; get; }
        public OptUnitType TargetType { set; get; }
        public int Counter { set; get; }

        public OptTypeOfLinks(OptUnitType sourceType, OptUnitType targetType)
        {
            SourceType = sourceType;
            TargetType = targetType;
            Counter = 1;
        }
    } 

    public class OptLinksCounter
    {
        public List<OptTypeOfLinks> ListOfLinks { set; get; }

        public OptLinksCounter()
        {
            ListOfLinks = new List<OptTypeOfLinks>();
        }

        public void AddLink(OptUnitType sourceType, OptUnitType targetType)
        {
            int index = -1;
            for (int i = 0; i < ListOfLinks.Count; i++)
                if ((sourceType == ListOfLinks[i].SourceType) && (targetType == ListOfLinks[i].TargetType))
                {
                    index = i;
                    break;
                }

            if (index == -1)
                ListOfLinks.Add(new OptTypeOfLinks(sourceType, targetType));
            else
                ListOfLinks[index].Counter++;
        }
    }
}
