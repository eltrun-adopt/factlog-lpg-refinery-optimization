﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TuprasOpt
{
    public class Root
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Definition Definition { get; set; }
    }

    public class Parameter
    {
        public string Symbol { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public double Value { get; set; }
    }

    public class Stock
    {
        public double Quantity { get; set; }
        public double ExtraQuantity { get; set; }
        public int Consume { get; set; }
        public bool Enabled { get; set; }
    }

    public class Node
    {
        [JsonProperty("$Type")]
        public string Type { get; set; }
        public int SpecificationMethod { get; set; }
        public string ScriptSource { get; set; }
        public List<Parameter> Parameters { get; set; }
        public string Stage { get; set; }
        public bool Solved { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Stock> Stocks { get; set; }
        public bool? OneToOne { get; set; }
    }

    public class Flow
    {
        public string Resource { get; set; }
        public string Name { get; set; }
        public double Quantity { get; set; }
        public bool Manual { get; set; }
        public bool Calculated { get; set; }
        public string Formula { get; set; }
        public double Factor { get; set; }
    }

    public class Link
    {
        public string Source { get; set; }
        public string Target { get; set; }
        public List<Flow> Flows { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class Resource
    {
        public string Unit { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class Definition
    {
        public List<Node> Nodes { get; set; }
        public List<Link> Links { get; set; }
        public object Stages { get; set; }
        public List<Resource> Resources { get; set; }
        public List<Parameter> Parameters { get; set; }
        public double Tolerance { get; set; }
    }
}
