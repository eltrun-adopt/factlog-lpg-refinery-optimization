﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/

using System;
using System.IO;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Google.OrTools.LinearSolver;
using System.Collections.Generic;

namespace TuprasOpt
{
    public class ManageRecovery
    {
        public string RunOptimizationEngine(Root pm, ProcessInstance pi, List<OperationalScenarioParameters> losp, ref int step)
        {
            string result = "";
            try
            {            // --- Deserialize objects ---
                string solverStr = "CBC_MIXED_INTEGER_PROGRAMMING";
                var watch = System.Diagnostics.Stopwatch.StartNew();
                // --- Initialize class model ----
                OptStructure opt = new OptStructure();
                opt.InitializeProcessModel(pm);                                         // Read process schema
                opt.Instance = pi;
                opt.InitializeInputOutput(pi);                                          // Instantiate process instance
                opt.InitializeOperationalScenarios(losp);                               // Instantiate operational scenarios
                opt.RunParetoFilter();
                opt.CreateSolver(solverStr);                                            // --- Initialize and solve MILP ----
                opt.InstantiateMILPvariables();                                         // Build MILP variables            
                opt.BuildMILPconstraints();                                             // Build MILP constraints                                 
                opt.BuildMILPobjective();                                               // Build objective
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                opt.Sol.SolKPIs.TimeToInitializeMillisec = Convert.ToDouble(elapsedMs);
                opt.SolveMILP();                                                        // Run solver to solve MIP model and return results
                result = JsonConvert.SerializeObject(opt.Sol);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }

            return result;
        }
    }
}
