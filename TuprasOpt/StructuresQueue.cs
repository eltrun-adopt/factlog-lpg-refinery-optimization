﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using Newtonsoft.Json.Linq;

namespace TuprasOpt
{
    public class QueueInput
    {
        public string uuid;
        public JObject data; /** The json data of the job. */
        public JObject model; /** The json data of the job. */
        public JObject operational_scenario; /** The json data of the job. */
    }

    public class QueueOutput
    {
        public string uuid;
        public long produced_at; /** The result production timestamp in millis. */
        public JObject data; 	/** The json data of the result. */
    }

    public class QueueInputData
    {
        public ProcessInstance PI { set; get; }
        public ListOperationalScenarios OS { set; get; }
        public Root PM { set; get; }
    }
}
