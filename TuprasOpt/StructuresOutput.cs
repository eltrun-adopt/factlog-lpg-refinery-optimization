﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using System.Collections.Generic;

namespace TuprasOpt
{
    public class Solution
    {
        public List<SelectedScenario> SolutionScenarios { set; get; }
        public double TotalEnergy { set; get; }
        public List<SpecsKPIs> OutputKPIs { set; get; }
        public SolutionKPIs SolKPIs { set; get; }

        public Solution()
        {
            SolutionScenarios = new List<SelectedScenario>();
            SolKPIs = new SolutionKPIs();
            OutputKPIs = new List<SpecsKPIs>();
        }
    }

    public class ErrorMessage
    {
        public string Error { set; get; }

        public ErrorMessage(string errorMessage)
        {
            Error = errorMessage;
        }
    }

    public class SelectedScenario
    {
        public string NodeID { set; get; }
        public string ScenarioID { set; get; }
        public string OptID { set; get; }
    }

    public class SolutionKPIs
    {
        public bool FoundSolution { set; get; }
        public double TimeToSolveMillisec { set; get; }
        public double TimeToInitializeMillisec { set; get; }
    }

    public class SpecsKPIs
    {
        public string OutputNodeID { set; get; }
        public double Quantity { set; get; }
        public double SUperc { set; get; }
        public double C2perc { set; get; }
        public double C5perc { set; get; }
        public double C2C5perc { set; get; }
    }
}
