# factlog: LPG Production Process Optimization


## Description
This code can be applied to optimize the LPG
Production process. The role of optimization is to handle the recovery to on-specs LPG
production in the most energy efficient way. To do so, this code identifies the
most energy efficient combination of operational scenarios for all process units involved in
the LPG purification process and proposes the settings combinations to the shopfloor 
to take the appropriate decision and action. This case was solved utilizing a
MIP model, based on a typical flow, and blending modelling approach that also incorporates
a binary decision variable for each operational scenario of each process unit. This approach
enables the optimization engine to take under consideration all involved units in the process,
something that also pushed the boundaries of research (as current solutions focus on single
unit optimization). Through experimentation we found that the behavior of our proposed
approach given different time horizons for recovery will be suitable for real field application.

## Authors
The person who contributed to this project is Dr Pavlos Eirinakis.

## Acknowledgement
This research was funded by the H2020 FACTLOG project, which has received funding
from the European Union’s Horizon 2020 programme under grant agreement No. 869951.
<https://www.factlog.eu/>

## License
factlog-lpg-refinery-optimization (c) by the University of Piraeus, Greece.

factlog-lpg-refinery-optimization is licensed under a
Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

You should have received a copy of the license along with this
work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.