﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using System;
using System.Collections.Generic;
using System.Text;
using TuprasOpt;

namespace TuprasOptConsole
{
    public class FlowScenario
    {
        public double FlowRate { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class LinkScenario
    {
        public string LinkID { get; set; }
        public double CAP_ij { get; set; }
        public List<FlowScenario> FlowScenarios { get; set; }
    }

    public class PSM_SCE
    {
        public string NodeID { get; set; }
        public string ScenarioID_i_s { get; set; }
        public double E_i_s { get; set; }
        public List<LinkScenario> LinkScenarios { get; set; }
    }


    public class Input_OperScenParams
    {
        public List<PSM_SCE> Input_OperScen { set; get; }
        public List<Parameters> Input_Params { set; get; }
    }
}
