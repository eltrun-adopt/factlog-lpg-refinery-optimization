﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using TuprasOpt;

namespace TuprasOptConsole
{
    public class ManageData
    {
        // PREPARE INPUT DATA
        public string UnifyScenarioFilesIntoSingle(string basicFolder)
        {
            string folderPath = basicFolder + @"\Scenarios\";
            string scenariosPath = basicFolder + @"\all_scenarios.json";

            List<PSM_SCE> listOfScenarios = new List<PSM_SCE>();
            foreach (string file in Directory.EnumerateFiles(folderPath, "*.json"))
            {
                string scenStr = File.ReadAllText(file);
                List<PSM_SCE> psm_sce = JsonConvert.DeserializeObject<List<PSM_SCE>>(scenStr);
                for (int i = 0; i < psm_sce.Count; i++)
                    listOfScenarios.Add(psm_sce[i]);
            }
            string scenarios = JsonConvert.SerializeObject(listOfScenarios);
            File.WriteAllText(scenariosPath, scenarios);

            return scenariosPath;
        }

        public string UnifyParameterFilesIntoSingle(string basicFolder)
        {
            string folderPath = basicFolder + @"\Parameters\";
            string parametersPath = basicFolder + @"\all_parameters.json";

            List<Parameters> listOfParameters = new List<Parameters>();
            foreach (string file in Directory.EnumerateFiles(folderPath, "*.json"))
            {
                string parStr = File.ReadAllText(file);
                List<Parameters> param = JsonConvert.DeserializeObject<List<Parameters>>(parStr);
                for (int i = 0; i < param.Count; i++)
                    listOfParameters.Add(param[i]);
            }
            string paramets = JsonConvert.SerializeObject(listOfParameters);
            File.WriteAllText(parametersPath, paramets);

            return parametersPath;
        }

        public string UnifyScenariosParametersIntoSingle(string unifiedPath, string scenariosPath, string parametersPath)
        {
            string scenStr = File.ReadAllText(scenariosPath);
            List<PSM_SCE> psm_sce = JsonConvert.DeserializeObject<List<PSM_SCE>>(scenStr);
            string parStr = File.ReadAllText(parametersPath);
            List<Parameters> psm_params = JsonConvert.DeserializeObject<List<Parameters>>(parStr);

            Input_OperScenParams iosp = new Input_OperScenParams();
            iosp.Input_OperScen = psm_sce;
            iosp.Input_Params = psm_params;
            string scenoper = JsonConvert.SerializeObject(iosp);
            File.WriteAllText(unifiedPath, scenoper);

            return unifiedPath;
        }

        public void PrepareQueueInput(string queueInputPath, string modelPath, string unifiedPath, string procInstancePath)
        {
            QueueInput qi = new QueueInput();
            string modelStr = File.ReadAllText(modelPath);
            JObject jmodel = JObject.Parse(modelStr);
            string unifiedStr = File.ReadAllText(unifiedPath);
            JObject joperscen = JObject.Parse(unifiedStr);
            string dataStr = File.ReadAllText(procInstancePath);
            JObject jdata = JObject.Parse(dataStr);

            qi.uuid = System.Guid.NewGuid().ToString();
            qi.model = jmodel;
            qi.operational_scenario = joperscen;
            qi.data = jdata;

            string queueInputStr = JsonConvert.SerializeObject(qi);
            File.WriteAllText(queueInputPath, queueInputStr);
        }

        public List<OperationalScenarioParameters> PrepareOperScenData(Input_OperScenParams iosp)
        {
            List<OperationalScenarioParameters> listOfOSP = new List<OperationalScenarioParameters>();

            List<PSM_SCE> listScenarios = iosp.Input_OperScen;
            List<Parameters> listParams = iosp.Input_Params;

            for (int i = 0; i < listParams.Count; i++)
            {
                OperationalScenarioParameters osp = new OperationalScenarioParameters();
                osp.OperationalScenarioID = listParams[i].ScenarioID_i_s;
                osp.NodeID = listParams[i].NodeID;
                osp.OperParams = listParams[i];
                listOfOSP.Add(osp);
            }

            for (int i = 0; i < listOfOSP.Count; i++)
            {
                OperationalScenario os = new OperationalScenario();
                int ind = -1;
                for (int j = 0; j < listScenarios.Count; j++)
                    if (listScenarios[j].ScenarioID_i_s == listOfOSP[i].OperationalScenarioID)
                    {
                        ind = j;
                        break;
                    }

                if (ind == -1)
                {
                    Console.WriteLine("Error - missing scenario");
                    Console.ReadLine();
                }
                else
                {
                    os.NodeID = listScenarios[ind].NodeID;
                    os.ScenarioID_i_s = listScenarios[ind].ScenarioID_i_s;
                    os.E_i_s = listScenarios[ind].E_i_s;
                    os.LinkScenarios = new List<TuprasOpt.LinkScenario>();
                    for (int s = 0; s < listScenarios[ind].LinkScenarios.Count; s++)
                    {
                        TuprasOpt.LinkScenario sc = new TuprasOpt.LinkScenario();
                        sc.LinkID = listScenarios[ind].LinkScenarios[s].LinkID;
                        //sc.CAP_ij = 1000 * listScenarios[ind].LinkScenarios[s].CAP_ij;
                        sc.CAP_ij = 10000;
                        double valueSU = 0;
                        double valueC2 = 0;
                        double valueC5 = 0;
                        double valueF = 0;
                        for (int f = 0; f < listScenarios[ind].LinkScenarios[s].FlowScenarios.Count; f++)
                            if (listScenarios[ind].LinkScenarios[s].FlowScenarios[f].Name == "LPG")
                                valueF = listScenarios[ind].LinkScenarios[s].FlowScenarios[f].FlowRate;
                            else if (listScenarios[ind].LinkScenarios[s].FlowScenarios[f].Name == "C2")
                                valueC2 = listScenarios[ind].LinkScenarios[s].FlowScenarios[f].FlowRate;
                            else if (listScenarios[ind].LinkScenarios[s].FlowScenarios[f].Name == "C5")
                                valueC5 = listScenarios[ind].LinkScenarios[s].FlowScenarios[f].FlowRate;
                            else if (listScenarios[ind].LinkScenarios[s].FlowScenarios[f].Name == "S")
                                valueSU = listScenarios[ind].LinkScenarios[s].FlowScenarios[f].FlowRate;
                        sc.PF_i_j_s = Math.Round((100 + valueF) / 100, 4);
                        sc.PSU_i_j_s = Math.Round((100 + valueSU) / 100, 4);
                        sc.PC2_i_j_s = Math.Round((100 + valueC2) / 100, 4);
                        sc.PC5_i_j_s = Math.Round((100 + valueC5) / 100, 4);
                        //CheckValidateValues(sc);

                        os.LinkScenarios.Add(sc);
                    }
                }
                listOfOSP[i].OperScen = os;
            }

            return listOfOSP;
        }

        public void CheckValidProcessInstance(ProcessInstance pi)
        {
            bool invalid = false;
            string message = "Invalid Process Instance: ";
            if (pi.InputFeeds == null)
            {
                invalid = true;
                message += " *Missing input feeds";
            }
            if (pi.OutputTanks == null)
            {
                invalid = true;
                message += " *Missing output tank";
            }
            if ((pi.Specs.C2 == 0) && (pi.Specs.C5 == 0) && (pi.Specs.SU == 0) && (pi.Specs.C2C5 == 0))
            {
                invalid = true;
                message += " *Missing specs";
            }
            if (pi.Settings.Horizon == 0)
            {
                invalid = true;
                message += " *Missing horizon";
            }

            if (invalid)
                throw new Exception(message);
        }


        // For testing purposes
        public void CheckValidateValues(TuprasOpt.LinkScenario sc)
        {
            if (sc.PSU_i_j_s > 1)
                sc.PSU_i_j_s = 1;
            if (sc.PC2_i_j_s > 1)
                sc.PC2_i_j_s = 1;
            if (sc.PC5_i_j_s > 1)
                sc.PC5_i_j_s = 1;
            if (sc.PF_i_j_s > 1)
                sc.PF_i_j_s = 1;
            if (sc.PSU_i_j_s <= 0)
                sc.PSU_i_j_s = 0.001;
            if (sc.PC2_i_j_s <= 0)
                sc.PC2_i_j_s = 0.001;
            if (sc.PC5_i_j_s <= 0)
                sc.PC5_i_j_s = 0.001;
            if (sc.PF_i_j_s <= 0.001)
                sc.PF_i_j_s = 0.001;
        }
    }
}
