﻿/*    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.

    factlog-lpg-refinery-optimization is licensed under a
    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

    You should have received a copy of the license along with this
    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
*/
    
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TuprasOpt;

namespace TuprasOptConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // ------------ RUN QUEUE -----------------
            string host = Environment.GetEnvironmentVariable("RB_HOST");
            string username = Environment.GetEnvironmentVariable("RB_USER");
            string password = Environment.GetEnvironmentVariable("RB_PASS");
            ConnectionFactory factory = new ConnectionFactory();
            factory.HostName = host;
            factory.UserName = username;
            factory.Password = password;
            factory.VirtualHost = "/";
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    ManageData md = new ManageData();
                    ManageRecovery mr = new ManageRecovery();
                    QueueInput qi = JsonConvert.DeserializeObject<QueueInput>(message);
                    string uuid = qi.uuid;
                    string result = "";
                    int step = 0;
                    bool catchError = false;
                    try
                    {
                        Root pm = qi.model.ToObject<Root>();
                        Input_OperScenParams iosp = qi.operational_scenario.ToObject<Input_OperScenParams>();
                        ProcessInstance pi = qi.data.ToObject<ProcessInstance>();
                        md.CheckValidProcessInstance(pi);
                        step++;
                        List<OperationalScenarioParameters> losp = md.PrepareOperScenData(iosp);
                        step++;
                        result = mr.RunOptimizationEngine(pm, pi, losp, ref step);
                    }
                    catch (Exception e)
                    {
                        result = e.Message.ToString() + " - Step " + step.ToString();
                        catchError = true;
                    }
                    QueueOutput qo = new QueueOutput();
                    qo.uuid = uuid;
                    qo.produced_at = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
                    if (!catchError)
                        qo.data = JObject.FromObject(JsonConvert.DeserializeObject<Solution>(result));
                    else
                        qo.data = JObject.FromObject(new ErrorMessage(result));
                    string outputStr = JsonConvert.SerializeObject(qo);
                    var bodyRet = Encoding.UTF8.GetBytes(outputStr);
                    channel.BasicPublish(exchange: "opt-result", routingKey: "process-flow-opt", basicProperties: null, body: bodyRet);
                };
                channel.BasicConsume(queue: "process-flow-opt_job", autoAck: true, consumer: consumer);
                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }

            //// ----------------- TEST WITHOUT QUEUE ---------------
            //Testing();
        }

        static void Testing()
        {
            // ----------------- TUPRAS Data Preparation ---------
            TestDataPreparation();
            // ----------------- RUN Console ----------------
            RunAppTest_Console();
        }

        static void RunAppTest_Console()
        {
            // ------------ RUN Console ----------------
            string basicFolder = Directory.GetCurrentDirectory();
            string queueInputPath = basicFolder + @"\queue_input.json";
            string message = File.ReadAllText(queueInputPath); // get queue input string
            ManageData md = new ManageData();
            ManageRecovery mpm = new ManageRecovery();
            QueueInput qi = JsonConvert.DeserializeObject<QueueInput>(message);
            string uuid = qi.uuid;
            string result = "";
            int step = 0;
            bool catchError = false;
            try
            {
                Root pm = qi.model.ToObject<Root>();
                Input_OperScenParams iosp = qi.operational_scenario.ToObject<Input_OperScenParams>();
                ProcessInstance pi = qi.data.ToObject<ProcessInstance>();
                md.CheckValidProcessInstance(pi);
                step++;
                List<OperationalScenarioParameters> losp = md.PrepareOperScenData(iosp);
                step++;
                result = mpm.RunOptimizationEngine(pm, pi, losp, ref step);
            }
            catch (Exception e)
            {
                result = e.Message.ToString() + " - Step " + step.ToString();
                catchError = true;
            }
            QueueOutput qo = new QueueOutput();
            qo.uuid = uuid;
            qo.produced_at = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
            if (!catchError)
                qo.data = JObject.FromObject(JsonConvert.DeserializeObject<Solution>(result));
            else
                qo.data = JObject.FromObject(new ErrorMessage(result));
            string outputStr = JsonConvert.SerializeObject(qo);
        }

        static void TestDataPreparation()
        {
            // ----------------- DATA PREPARATION SETTING ------------------------------------
            string basicFolder = Directory.GetCurrentDirectory();
            string modelPath = basicFolder + @"\PSM_process_model.json";         // Get modelPath (PSM)
            string unifiedPath = basicFolder + @"\all_PSM_scen_param_input.json";
            string procInstancePath = basicFolder + @"\opt-request-new.json";
            //
            string queueInputPath = basicFolder + @"\queue_input.json";

            ManageData md = new ManageData();
            string scenariosPath = md.UnifyScenarioFilesIntoSingle(basicFolder);
            string parametersPath = md.UnifyParameterFilesIntoSingle(basicFolder);
            md.UnifyScenariosParametersIntoSingle(unifiedPath, scenariosPath, parametersPath);
            md.PrepareQueueInput(queueInputPath, modelPath, unifiedPath, procInstancePath);
        }
    }
}
