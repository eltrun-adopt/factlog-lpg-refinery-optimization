#    factlog-lpg-refinery-optimization (c) by the University of Piraues, Greece.
#
#    factlog-lpg-refinery-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build-env

ENV RB_HOST $RB_HOST
ENV RB_PORT $RB_PORT
ENV RB_USER $RB_USER

WORKDIR ./tupras

# Copy everything and build
COPY ./ ./
RUN cd ./TuprasOptConsole && dotnet build -c Release -o out

ENTRYPOINT /tupras/TuprasOptConsole/out/TuprasOptConsole
